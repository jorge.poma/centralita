const express = require('express')
const path = require('path')
const body_parser = require('body-parser')

const app= express();
const port = Math.floor(Math.random() * (4000 - 3000) + 3000);
const routes = require("./routes/routes")

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs')

app.use((req, res, next)=>{
  next()
})
app.use(body_parser.json())
app.use(body_parser.urlencoded({extended: true}))

app.use(routes)

app.use(express.static(path.join(__dirname, 'public')));


app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})

/* *************************************************

cliente socket

*************************************************** */






