-module(server).
-export([start/1]).

start(Port) ->
    spawn(fun() -> 
        ets:new(table_phones, [bag, public, named_table]),
        srv_init(Port)
    end).

%set_phones(N) ->
%    Numeros = [].

srv_init(Port) ->
    
    Opts = [{reuseaddr, true}, {active, false}],
    {ok, Socket} = gen_tcp:listen(Port, Opts),
    srv_loop(Socket).

srv_loop(Socket) ->
    
    {ok, SockCli} = gen_tcp:accept(Socket),
    Pid = spawn(fun() -> worker_loop(SockCli) end),
    %io:format("Socket:~w",[Socket]),
    %io:format("SockCli:~w",[SockCli]),
    io:format("~p",[self()]),
    gen_tcp:controlling_process(SockCli, Pid),
    inet:setopts(SockCli, [{active, true}]),
    srv_loop(Socket).

worker_loop(Socket) ->
    receive
        {tcp, Socket, <<"llamar=",X/binary>>} ->
            Val = list_to_integer(binary_to_list(X)),
            %Salida = io_lib:format("Result sq: ~w",list_to_binary(integer_to_list(Val))),
            gen_tcp:send(Socket, "Result sq:"++list_to_binary(integer_to_list(Val))),
            worker_loop(Socket);

        {tcp, Socket, Msg} ->
            io:format("Recibido ~p: ~p~n", [self(), Msg]),
            [_MiNum,Mensaje,Valor] = string:split(Msg,":",all),
             io:format("arreglo: ~p, ~p, ~p~n", [_MiNum,Mensaje,Valor]),
            case Mensaje of
                "registrar" ->
                    %Salida = io_lib:format("Ingrese numero: ~s", [Msg]);
                    ets:insert(table_phones, {Valor, Socket}),
                    Salida = io_lib:format("Guardado: ~p", [Valor]),
                    gen_tcp:send(Socket, Salida);

                "llamar" ->
                    %Salida = io_lib:format("Ingrese numero: ~s", [Msg]);
                    Out = ets:lookup(table_phones, Valor),
                    [{M,S}] = Out,
                    io:format("llamando~n"),
                    gen_tcp:send(Socket, "Llamando a: "++M),
                    gen_tcp:send(S, "Recibiendo llamada de:");

                "contestar" ->
                    %Salida = io_lib:format("Ingrese numero: ~s", [Msg]);
                    Out = ets:lookup(table_phones, Valor),
                    [{M,S}] = Out,
                    io:format("llamada en curso...~n"),
                    gen_tcp:send(Socket, "Llamanda en curso con: "++M),
                    gen_tcp:send(S, "Llamanda en curso con: "++_MiNum);

                "colgar" ->
                    %Salida = io_lib:format("Ingrese numero: ~s", [Msg]);
                    Out = ets:lookup(table_phones, Valor),
                    [{M,S}] = Out,
                    io:format("Fin de la llamada...~n"),
                    gen_tcp:send(Socket, "Colgado por: "++M),
                    gen_tcp:send(S, "Colgado por: "++_MiNum);

                _ ->
                    
                    io_lib:format("Opcion no valida!!: ~s", [""])
                    %Out = ets:lookup(table_phones, Msg),
                    %Out2 = ets:match(table_phones,'$2'),
                    %if 
                    %    Out =:= [] ->
                    %        ets:insert(table_phones, {Msg, Socket}),
                    %        io:format("Guardado: ~p", [Out]),
                    %        gen_tcp:send(Socket, Salida);
                    %true ->
                    %        [{M,S}] = Out,
                    %        io:format("llamando~n"),
                    %        gen_tcp:send(Socket, "Llamando a:"++M),
                    %        gen_tcp:send(S, "Recibiendo llamada")
                    %end
                    
                    %io:format("Guardado: ~p", [Out2])
                end,
                worker_loop(Socket);
            %timer:sleep(5000), %% 5 segundos de espera
            
        {tcp_closed, Socket} ->
            io:format("Finalizado.~n");
            Any ->
            io:format("Mensaje no reconocido: ~p~n", [Any])
        end.

%store_data(Out, Msg, Socket) ->
    
    